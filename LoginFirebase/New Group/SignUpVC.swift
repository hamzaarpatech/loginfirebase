//
//  SignUpVC.swift
//  LoginFirebase
//
//  Created by Hamza on 2/21/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class SignUpVC: UIViewController {
    
    @IBOutlet weak var firstNameTextfield: UITextField!
    @IBOutlet weak var lastNameTextfield: UITextField!
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passTextField: UITextField!
    @IBOutlet weak var signupBt: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSignupStyling()
    }
    
    func setupSignupStyling(){
        errorLabel.alpha = 0
        Utilities.styleTextField(firstNameTextfield)
        Utilities.styleTextField(lastNameTextfield)
        Utilities.styleTextField(emailTextfield)
        Utilities.styleTextField(passTextField)
        Utilities.styleFilledButton(signupBt)
    }
    
    @IBAction func signUpTapped(_ sender: Any) {
        //Validate the fields
        let error = validateFields()
        if error != nil {
            showError(error!)
        }
        else{
            //Create the users
            let first = self.firstNameTextfield.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let last = self.lastNameTextfield.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let email = self.emailTextfield.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let pass = self.passTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)

            
            Auth.auth().createUser(withEmail: email, password: pass) { (result, err) in
                //check for error
                if err != nil {
                    self.showError(err!.localizedDescription)
                    print("Error in creating users")
                }
                 //user created successfully , now store it in documents firestore
                else{
                    let db = Firestore.firestore()
                    db.collection("users").document(result!.user.uid).setData(
                        ["firstname": first , "lastname":last , "uid": result!.user.uid]
                    )
                    self.transitionToHomeScreen()
                }
            }
        }
    }
    
    //check if fields correct pass nil else return error msg
    func validateFields() -> String? {
        //Check fields are fill and remove white spaces
        if firstNameTextfield.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
           lastNameTextfield.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
           emailTextfield.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
           passTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
           return "Please fill all fields"
        }
        //Check email regex
//        let cleanedEmail = emailTextfield.text!.trimmingCharacters(in: .whitespacesAndNewlines)
//        if Utilities.isEmailValid(cleanedEmail) == false {
//            return "Type valid email"
//        }
        
        //Check password regex
        let cleanedPassword = passTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        if Utilities.isPasswordValid(cleanedPassword) == false {
            return "Type strong password"
        }
        return nil
    }
    
    func showError(_ message:String){
        errorLabel.text = message
        errorLabel.alpha = 1
    }
    
    func transitionToHomeScreen(){
       let homeViewController = storyboard?.instantiateViewController(identifier: Constants.Storyboard.homeStoryId) as? HomeVC
        view.window?.rootViewController = homeViewController
        view.window?.makeKeyAndVisible()
    }
}
