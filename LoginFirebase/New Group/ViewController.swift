//
//  ViewController.swift
//  LoginFirebase
//
//  Created by Hamza on 2/21/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit
import AVKit

class ViewController: UIViewController {
    
    var videoPlayer : AVPlayer!
    var videoPlayerLayer : AVPlayerLayer!
    @IBOutlet weak var signupBt: UIButton!
    @IBOutlet weak var loginBt: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupStyling()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupVideo()
    }
    
    func setupVideo(){
        
       // get path from resource bundle
        let bundlepath = Bundle.main.path(forResource: "video1", ofType: ".mp4")
        guard bundlepath != nil else { return }
       
        // create a url
        let url = URL(fileURLWithPath: bundlepath!)
        
       // create video player item
        let item = AVPlayerItem(url: url)
        
       // create the player
        videoPlayer = AVPlayer(playerItem: item)
        
       // create the layer
        videoPlayerLayer = AVPlayerLayer(player: videoPlayer!)
        
       // adjust the size and frame
        videoPlayerLayer?.frame = CGRect(x: -self.view.frame.width*1.5, y: 0, width: self.view.frame.width*4, height: self.view.frame.height)
        view.layer.insertSublayer(videoPlayerLayer!, at: 0)
        
       // add it to view and play it
        videoPlayer.playImmediately(atRate: 0.6)
    }

    func setupStyling(){
        Utilities.styleFilledButton(signupBt)
        Utilities.styleHollowButton(loginBt)
    }
}
