//
//  LoginVC.swift
//  LoginFirebase
//
//  Created by Hamza on 2/21/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase

class LoginVC: UIViewController {

    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var passText: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLoginStyling()
    }
    
    func setupLoginStyling(){
        errorLabel.alpha = 0
        Utilities.styleTextField(emailText)
        Utilities.styleTextField(passText)
        Utilities.styleFilledButton(loginButton)
    }
    
    @IBAction func loginTapped(_ sender: Any) {
    
        // create cleaned version of textfield
        let email = self.emailText.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let pass = self.passText.text!.trimmingCharacters(in: .whitespacesAndNewlines)

       //  signing the user in
        Auth.auth().signIn(withEmail: email, password: pass ) { (result, error) in
            // couldnot sign in
            if error != nil {
                self.errorLabel.text = error?.localizedDescription
                self.errorLabel.alpha = 1
            }
            else{
                let homeViewController = self.storyboard?.instantiateViewController(identifier: Constants.Storyboard.homeStoryId) as? HomeVC
                self.view.window?.rootViewController = homeViewController
                self.view.window?.makeKeyAndVisible()
            }
        }
    }
}
