//
//  HomeVC.swift
//  LoginFirebase
//
//  Created by Hamza on 2/21/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase

class HomeVC: UIViewController {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var logoutBt: UIButton!
    var uid = ""
    
    func userRoleListener() {
          guard let userUid = Auth.auth().currentUser?.uid else { return }
        print(userUid)
          let db = Firestore.firestore()
      let docRef = db.collection("users").document(userUid)
       
          docRef.getDocument { (snapshot, err) in
              if let data = snapshot?.data() {
                  let first = data["firstname"]!
                  let second = data["lastname"]!
                  self.nameTextField.text = "Welcome , \(first) \(second)"
              } else {
                  print("Couldn't find the document")
              }
          }
      }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userRoleListener()
        Utilities.styleTextField(nameTextField)
        Utilities.styleFilledButton(logoutBt)
    }
  
    @IBAction func logoutBtPressed(_ sender: UIButton) {
        try! Auth.auth().signOut()
        let mainVC = storyboard?.instantiateViewController(identifier: Constants.Storyboard.mainVCid) as? ViewController
        view.window?.rootViewController = mainVC
        view.window?.makeKeyAndVisible()
    }
}
