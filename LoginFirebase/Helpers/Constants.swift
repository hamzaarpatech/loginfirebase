//
//  Constants.swift
//  LoginFirebase
//
//  Created by Hamza on 2/21/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import Foundation

struct Constants{
    struct Storyboard{
        static let homeStoryId = "HomeVC"
        static let mainVCid = "MainVC"
    }
    
}
